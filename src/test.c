#include "shape.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

/* I had some doubts while testing on provided files, so I took well-known coords and distance
    // MSK->SPB 
    SHAPE_COORDS_XY spb = {
        .y = 59.937500, .x = 30.308611
    };
    SHAPE_COORDS_XY msk = {
        .y = 55.751244, .x = 37.618423
    };
    printf("MSK->SPB %.3f\n", SHAPE_COORDSXY_MEASURE(msk, spb));
    // got 634942.353, looks decent
*/

int main(int argc, char **argv) {
    int shp_fd, dbf_fd;
    void *shp_buf = NULL, *dbf_buf = NULL;
    size_t shp_buf_size, dbf_buf_size = 0;
    struct stat st;
    int ret;
    shp_fd = open(argv[1], O_RDONLY);
    if(shp_fd < 0)
        goto end;
    fstat(shp_fd, &st);
    shp_buf_size = st.st_size;
    shp_buf = calloc(1, shp_buf_size);
    ret = read(shp_fd, shp_buf, shp_buf_size);
    close(shp_fd);
    if(argc > 2) {
        dbf_fd = open(argv[2], O_RDONLY);
        if(dbf_fd < 0)
            goto end;
        fstat(dbf_fd, &st);
        dbf_buf_size = st.st_size;
        dbf_buf = calloc(1, dbf_buf_size);
        ret = read(dbf_fd, dbf_buf, dbf_buf_size);
        close(dbf_fd);
    }
    Shape *shape = shape_create();
    ret = shape_load_data(shape, shp_buf, shp_buf_size, dbf_buf, dbf_buf_size);
    //printf("Status: %d\n", ret);
    if(ret == SHAPE_STATUS_OK) {
        ShapeRecord *rec = shape_alloc_record();
        for(;;) {
            ret = shape_read_record(shape, &rec);
            //printf("Status: %d\n", ret);
            if(ret != SHAPE_STATUS_OK)
                break;
            char _tmp[4096];
            printf("Record Number: %d\n", rec->recordNumber);
            printf("ContentType: %d\n", rec->contentType);
            if(rec->contentType == SHAPE_TYPE_POINT) {
                printf("Point: (%.6f; %.6f)\n", SHAPERECORDPOINT(rec)->x, SHAPERECORDPOINT(rec)->y);
            } else if(rec->contentType == SHAPE_TYPE_MULTIPOINT) {
                printf("Number of Points: %d\n", SHAPERECORDMULTIPOINT(rec)->numPoints);
                printf("Points: %s\n", shape_utils_strcoordsxy(_tmp, SHAPERECORDMULTIPOINT(rec)->points, SHAPERECORDMULTIPOINT(rec)->numPoints));
            } else if(rec->contentType == SHAPE_TYPE_POLYLINE) {
                printf("Parts: %s\n", shape_utils_strparts(_tmp, SHAPERECORDPOLYLINE(rec)->parts, SHAPERECORDPOLYLINE(rec)->numParts));
                printf("Points: %s\n", shape_utils_strcoordsxy(_tmp, SHAPERECORDPOLYLINE(rec)->points, SHAPERECORDPOLYLINE(rec)->numPoints));
            } else if(rec->contentType == SHAPE_TYPE_POLYGON) {
                printf("Parts: %s\n", shape_utils_strparts(_tmp, SHAPERECORDPOLYGON(rec)->parts, SHAPERECORDPOLYGON(rec)->numParts));
                printf("Points: %s\n", shape_utils_strcoordsxy(_tmp, SHAPERECORDPOLYGON(rec)->points, SHAPERECORDPOLYGON(rec)->numPoints));
            }
            if(rec->attrs) {
                printf("Number of Attributes: %d\n", rec->numAttrs);
                printf("LINK_ID=\"%s\"\nST_NAME=\"%s\"\nFR_SPD_LIM=\"%s\"\nTO_SPD_LIM=\"%s\"\n",
                    shape_get_record_attr(rec, "LINK_ID", NULL, NULL, NULL),
                    shape_get_record_attr(rec, "ST_NAME", NULL, NULL, NULL),
                    shape_get_record_attr(rec, "FR_SPD_LIM", NULL, NULL, NULL),
                    shape_get_record_attr(rec, "TO_SPD_LIM", NULL, NULL, NULL)
                );
            }
        }
        free(rec);
    }
    shape_destroy(shape);
end:
    free(dbf_buf);
    free(shp_buf);
    return 0;
}