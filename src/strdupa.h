#ifndef SRTDUPA_H
#define STRDUPA_H

#define strdupa(str) ({                                               \
    const char *__strdupa_old = (str);                                \
    size_t __strdupa_len = strlen(__strdupa_old) + 1;                 \
    char *__strdupa_new = alloca(__strdupa_len);                      \
    (char *) memcpy(__strdupa_new, __strdupa_old, __strdupa_len);     \
})

#endif