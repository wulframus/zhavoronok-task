#ifndef SHAPE_H
#define SHAPE_H

#include <features.h>
#include <sys/types.h>

#define SHAPE_STATUS_ERR         -2
#define SHAPE_STATUS_EOF         -1
#define SHAPE_STATUS_OK           0
#define SHAPE_STATUS_INVALID_SHP  1
#define SHAPE_STATUS_INVALID_DBF  2

#define SHAPE_TYPE_MALFORMED     -1
#define SHAPE_TYPE_NULL           0
#define SHAPE_TYPE_POINT          1
#define SHAPE_TYPE_POLYLINE       3
#define SHAPE_TYPE_POLYGON        5
#define SHAPE_TYPE_MULTIPOINT     8
#define SHAPE_TYPE_POINTZ        11
#define SHAPE_TYPE_POLYLINEZ     13
#define SHAPE_TYPE_POLYGONZ      15
#define SHAPE_TYPE_MULTIPOINTZ   18
#define SHAPE_TYPE_POINTM        21
#define SHAPE_TYPE_POLYLINEM     23
#define SHAPE_TYPE_POLYGONM      25
#define SHAPE_TYPE_MULTIPOINTM   28
#define SHAPE_TYPE_MULTIPATCH    31

#define SHAPE_TYPE_IS_VALID(type) (       \
	(type) == SHAPE_TYPE_NULL          || \
	(type) == SHAPE_TYPE_POINT         || \
	(type) == SHAPE_TYPE_POLYLINE      || \
	(type) == SHAPE_TYPE_POLYGON       || \
	(type) == SHAPE_TYPE_MULTIPOINT    || \
	(type) == SHAPE_TYPE_POINTZ        || \
	(type) == SHAPE_TYPE_POLYLINEZ	   || \
	(type) == SHAPE_TYPE_POLYGONZ      || \
	(type) == SHAPE_TYPE_MULTIPOINT    || \
	(type) == SHAPE_TYPE_POINTM        || \
	(type) == SHAPE_TYPE_POLYLINEM     || \
	(type) == SHAPE_TYPE_POLYGONM      || \
	(type) == SHAPE_TYPE_MULTIPOINTM   || \
	(type) == SHAPE_TYPE_MULTIPATCH       \
)  

#define SHAPE_TYPE_IS_IMPLEMENTED(type) ( \
    (type) == SHAPE_TYPE_NULL          || \
	(type) == SHAPE_TYPE_POINT	       || \
	(type) == SHAPE_TYPE_POLYLINE      || \
	(type) == SHAPE_TYPE_POLYGON       || \
	(type) == SHAPE_TYPE_MULTIPOINT       \
)

#define SHAPE_TYPE_HAS_Z(type) (          \
	(type) == SHAPE_TYPE_POINTZ        || \
	(type) == SHAPE_TYPE_POLYLINEZ     || \
	(type) == SHAPE_TYPE_POLYGONZ      || \
	(type) == SHAPE_TYPE_MULTIPOINTZ      \
)

#define SHAPE_TYPE_HAS_M(type) (         \
	(type) == SHAPE_TYPE_POINTZ       || \
	(type) == SHAPE_TYPE_POLYLINEZ    || \
	(type) == SHAPE_TYPE_POLYGONZ     || \
	(type) == SHAPE_TYPE_MULTIPOINTZ  || \
	(type) == SHAPE_TYPE_POINTM       || \
	(type) == SHAPE_TYPE_POLYLINEM    || \
	(type) == SHAPE_TYPE_POLYGONM     || \
	(type) == SHAPE_TYPE_MULTIPOINTM     \
)

#define SHAPE_COORDS_XY struct { double x, y; }
#define SHAPE_COORDS_XYZ struct { double x, y, z; }

#define SHAPE_BOX_XY struct { double xMin, yMin, xMax, yMax; }
#define SHAPE_BOX_XYM struct { double xMin, yMin, xMax, yMax, mMim, mMax; }
#define SHAPE_BOX_XYZ struct { double xMin, yMin, zMin, xMax, yMax, zMax, mMin, mMax; }

struct _ShapeAttribute {
    char attrName[32];
    int attrType;
    size_t attrSize;
    size_t attrDecimalCount;
    const char *value;
};

typedef struct _ShapeAttribute ShapeAttribute;

struct _Shape {
    // [ Shape
    void *shp_buffer;
    size_t shp_buffer_size;
    void *shp_pbuffer;
    size_t shp_full_size;
    size_t shp_read_bytes;
    int shp_type;
    SHAPE_BOX_XYZ shp_box;
    // ]
    // [ dBase
    void *dbf_buffer;
    size_t dbf_buffer_size;
    void *dbf_pbuffer;
    size_t dbf_record_count;
    size_t dbf_record_size;
    size_t dbf_field_count;
    size_t dbf_full_size;
    size_t dbf_read_bytes;
    size_t _dbf_attrs;
    ShapeAttribute *dbf_attrs;
    int dbf_signature;
    int dbf_malformed;
    // ]
};


#define SHAPE_RECORD_COMMON \
    int recordNumber; \
    int contentType; \
    size_t contentLength; \
    size_t attrsLength; \
    int numAttrs; \
    ShapeAttribute *attrs; \
    int attrsMalformed; \
    size_t _attrs; \
    size_t _unused;

struct _ShapeRecord {
    SHAPE_RECORD_COMMON
};

// All
#define SHAPE_RECORD_UNUSED_PTR(rec) ((void *)(rec) + (rec)->_unused)
#define SHAPE_RECORD_ATTRS_PTR(rec) ((void *)(rec) + (rec)->_attrs)
// Polyline, Polygon
#define SHAPE_RECORD_PARTS_PTR(rec) ((void *)(rec) + (rec)->_parts)
// Multipoint, Polyline, Polygon
#define SHAPE_RECORD_POINTS_PTR(rec) ((void *)(rec) + (rec)->_points)

struct _ShapeRecordNull {
    SHAPE_RECORD_COMMON
};

struct _ShapeRecordPoint {
    SHAPE_RECORD_COMMON
    double x, y;
};

struct _ShapeRecordPolyline {
    SHAPE_RECORD_COMMON
    SHAPE_BOX_XY box;
    int numParts;
    int numPoints;
    int *parts;
    SHAPE_COORDS_XY *points;
    size_t _parts;
    size_t _points;
};

struct _ShapeRecordPolygon {
    SHAPE_RECORD_COMMON
    SHAPE_BOX_XY box;
    int numParts;
    int numPoints;
    int *parts;
    SHAPE_COORDS_XY *points;
    size_t _parts;
    size_t _points;
};

struct _ShapeRecordMultipoint {
    SHAPE_RECORD_COMMON
    SHAPE_BOX_XY box;
    int numPoints;
    SHAPE_COORDS_XY *points;
    size_t _points;
};

typedef struct _Shape Shape;
typedef struct _ShapeRecord ShapeRecord;
typedef struct _ShapeRecordNull ShapeRecordNull;
typedef struct _ShapeRecordPoint ShapeRecordPoint;
typedef struct _ShapeRecordPolyline ShapeRecordPolyline;
typedef struct _ShapeRecordPolygon ShapeRecordPolygon;
typedef struct _ShapeRecordMultipoint ShapeRecordMultipoint;

#define SHAPERECORDPOINT(rec) ((ShapeRecordPoint *) (rec))
#define SHAPERECORDMULTIPOINT(rec) ((ShapeRecordMultipoint *) (rec))
#define SHAPERECORDPOLYLINE(rec) ((ShapeRecordPolyline *) (rec))
#define SHAPERECORDPOLYGON(rec) ((ShapeRecordPolygon *) (rec))

__BEGIN_DECLS

Shape *shape_create(void);
void shape_destroy(Shape *shape);

int shape_load_data(Shape *shape, void *shp_buffer, size_t shp_buffer_size, void *dbf_buffer, size_t dbf_buffer_size);

ShapeRecord *shape_alloc_record(void);
void shape_free_record(ShapeRecord *rec);

int shape_read_record(Shape *shape, ShapeRecord **rec);
const char *shape_get_record_attr(ShapeRecord *rec, const char *attr, char *dst, size_t *attrsize, int *attrtype);

char *shape_utils_strparts(char *dst, const void *parts, size_t nparts);
char *shape_utils_strcoordsxy(char *dst, const void *coords, size_t ncoords);

__END_DECLS

#endif /* SHAPE_H */