#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include "shape.h"

#define degtorad(d) ( (d) * M_PI / 180.0 )
#define sqr(d) ( (d) * (d) )

// google says so
#define RADIUS_EARTH 6372800.0

// https://rosettacode.org/wiki/Haversine_formula
double haversine(double lat1, double lon1, double lat2, double lon2, double radius) {
    lat1 = degtorad(lat1); lon1 = degtorad(lon1);
    lat2 = degtorad(lat2); lon2 = degtorad(lon2);
    double dlat = lat2 - lat1;
    double dlon = lon2 - lon1;
    double a = sqr(sin(dlat/2.0)) + cos(lat1) * cos(lat2) * sqr(sin(dlon/2.0));
    return 2.0 * radius * asin(sqrt(a));
}

// I've found X represents longitude, Y — latitude
#define SHAPE_COORDSXY_MEASURE(p1, p2) haversine((p1).y, (p1).x, (p2).y, (p2).x, RADIUS_EARTH)

double shape_record_polyline_measure(ShapeRecordPolyline *rec) {
    double ret = 0.0;
    int i;
    for(i = 0; i < rec->numPoints - 1; i++)
        ret += SHAPE_COORDSXY_MEASURE(rec->points[i], rec->points[i+1]);
    return ret;
}

#define dbfntrim(str) ({ char *_p; (_p = strrchr((str), ' ')) ? (_p + 1) : (str); })
static inline char *dbfctrim(char *str, size_t str_size) {
    char *pstr = ((char *) str + str_size - 1);
    while(pstr >= str && *pstr == ' ')
        *(pstr--) = '\0';
    return str;
}

#define INFO_DATA_FILENAME "info_data.txt"
#define INFO_LOG_FILENAME "info_log.txt"

int main(int argc, char **argv) {
    int exit_status = EXIT_SUCCESS;

    int fd, ret;
    struct stat st;
    void *shp_buf = NULL, *dbf_buf = NULL;
    size_t shp_buf_size = 0, dbf_buf_size = 0;
    
    int info_log_fd, info_data_fd;
    char info_log_buf[1024], info_data_buf[1024];
    size_t info_log_buf_size, info_data_buf_size;

    info_log_fd = open(INFO_LOG_FILENAME, O_CREAT | O_WRONLY, 00644);
    if(info_log_fd < 0) {
        info_log_fd = STDERR_FILENO;
    } else {
        ret = lseek(info_log_fd, 0, SEEK_END);
    }

    info_data_fd = open(INFO_DATA_FILENAME, O_WRONLY | O_CREAT | O_TRUNC, 00644);
    if(info_data_fd < 0) {
        sprintf(info_log_buf, "Failed to open \"%s\": %s\n", INFO_DATA_FILENAME, strerror(errno));
        info_log_buf_size = strlen(info_log_buf);
        ret = write(info_log_fd, info_log_buf, info_log_buf_size);
        fsync(info_log_fd);
        exit_status = EXIT_FAILURE;
        goto end;
    }

    // load SHP file
    char shp_filename[256], dbf_filename[256];
    sprintf(shp_filename, "%s.shp", argv[1]);
    sprintf(dbf_filename, "%s.dbf", argv[1]);
    fd = open(shp_filename, O_RDONLY);
    if(fd < 0) {
        sprintf(info_log_buf, "Failed to open \"%s\": %s\n", shp_filename, strerror(errno));
        info_log_buf_size = strlen(info_log_buf);
        ret = write(info_log_fd, info_log_buf, info_log_buf_size);
        fsync(info_log_fd);
        exit_status = EXIT_FAILURE;
    }
    fstat(fd, &st);
    shp_buf_size = st.st_size;
    shp_buf = calloc(1, shp_buf_size);
    ret = read(fd, shp_buf, shp_buf_size);
    close(fd);
    // load DBF file
    fd = open(dbf_filename, O_RDONLY);
    if(fd < 0) {
        sprintf(info_log_buf, "Failed to open \"%s\": %s\n", dbf_filename, strerror(errno));
        info_log_buf_size = strlen(info_log_buf);
        ret = write(info_log_fd, info_log_buf, info_log_buf_size);
        fsync(info_log_fd);
        exit_status = EXIT_FAILURE;
        goto end;
    }
    fstat(fd, &st);
    dbf_buf_size = st.st_size;
    dbf_buf = calloc(1, dbf_buf_size);
    ret = read(fd, dbf_buf, dbf_buf_size);
    close(fd);
    // Create and fill shape structure
    Shape *shape = shape_create();
    ret = shape_load_data(shape, shp_buf, shp_buf_size, dbf_buf, dbf_buf_size);
    if(ret != SHAPE_STATUS_OK) {
        sprintf(info_log_buf, "Malformed SHP or DBF: %d\n", ret);
        info_log_buf_size = strlen(info_log_buf);
        ret = write(info_log_fd, info_log_buf, info_log_buf_size);
        fsync(info_log_fd);
        exit_status = EXIT_FAILURE;
    } else {
        ShapeRecord *rec;
        unsigned record_count = shape->dbf_record_count;
        sprintf(info_data_buf, "N = %u\n", record_count);
        info_data_buf_size = strlen(info_data_buf);
        ret = write(info_data_fd, info_data_buf, info_data_buf_size);
#ifdef EXTRA_TASK
        sprintf(info_data_buf, "// %5s %10s %11s %11s %11s %11s %3s %8s %3s %3s\n",
            "NN", "ID_num", "lat_start", "lon_start", "lat_end", "lon_end", "top", "length", "FSL", "TSL"
        );
        info_data_buf_size = strlen(info_data_buf);
        ret = write(info_data_fd, info_data_buf, info_data_buf_size);
        info_data_buf_size = strlen(info_data_buf);
#endif
        // store
        rec = shape_alloc_record();
        for(;;) {
            ret = shape_read_record(shape, &rec);
            if(ret == SHAPE_STATUS_EOF) {
                sprintf(info_log_buf, "Reached EOF\n");
                info_log_buf_size = strlen(info_log_buf);
                ret = write(info_log_fd, info_log_buf, info_log_buf_size);
                fsync(info_log_fd);
                break;
            }
            if(ret != SHAPE_STATUS_OK) {
                sprintf(info_log_buf, "Malformed data on %d: %d\n", rec->recordNumber, ret);
                info_log_buf_size = strlen(info_log_buf);
                ret = write(info_log_fd, info_log_buf, info_log_buf_size);
                fsync(info_log_fd);
                exit_status = EXIT_FAILURE;
                break;
            }
            if(rec->contentType != SHAPE_TYPE_POLYLINE) {
                sprintf(info_log_buf, "ShapeRecord is not in our range of interest: expected %d, got %d\n", rec->recordNumber, SHAPE_TYPE_POLYLINE, rec->contentType);
                info_log_buf_size = strlen(info_log_buf);
                ret = write(info_log_fd, info_log_buf, info_log_buf_size);
                fsync(info_log_fd);
                exit_status = EXIT_FAILURE;
                break;
            }
            // [ It's everything we need to finish the task, but I wanna play a little while I have a time :)           
            double lat_start, lon_start, lat_end, lon_end;
            int rec_number;
            double line_length;
            int num_vertices;
            const char *attr_LINK_ID, *attr_FR_SPD_LIM, *attr_TO_SPD_LIM;

            rec_number = rec->recordNumber;
            num_vertices = SHAPERECORDPOLYLINE(rec)->numPoints;
            lat_start = SHAPERECORDPOLYLINE(rec)->points[0].y;
            lon_start = SHAPERECORDPOLYLINE(rec)->points[0].x;
            lat_end = SHAPERECORDPOLYLINE(rec)->points[num_vertices - 1].y;
            lon_end = SHAPERECORDPOLYLINE(rec)->points[num_vertices - 1].x;
            attr_LINK_ID = shape_get_record_attr(rec, "LINK_ID", NULL, NULL, NULL);
            attr_FR_SPD_LIM = shape_get_record_attr(rec, "FR_SPD_LIM", NULL, NULL, NULL);
            attr_TO_SPD_LIM = shape_get_record_attr(rec, "TO_SPD_LIM", NULL, NULL, NULL);
            line_length = shape_record_polyline_measure(SHAPERECORDPOLYLINE(rec));
            // ]
#ifdef EXTRA_TASK
            // Because I can :)
            char _tmp[4096];
            const char *attr_ST_NAME; size_t attr_ST_NAME_size;
            char trail[4096];
            memset(trail, 0, sizeof(trail));
            attr_ST_NAME = shape_get_record_attr(rec, "ST_NAME", _tmp, &attr_ST_NAME_size, NULL);
            dbfctrim(_tmp, attr_ST_NAME_size);
            if(strlen(attr_ST_NAME) > 0) {
                sprintf(trail, " // %s", attr_ST_NAME);
            }
#endif
            // [ Store data
            sprintf(info_data_buf, "%08d %010d %11.6f %11.6f %11.6f %11.6f %3d %8.0f %3s %3s"
#ifdef EXTRA_TASK
                "%s"
#endif
                "\n",
                rec_number, atoi(dbfntrim(attr_LINK_ID)),
                lat_start, lon_start, lat_end, lon_end, num_vertices, line_length,
                dbfntrim(attr_FR_SPD_LIM), dbfntrim(attr_TO_SPD_LIM)
#ifdef EXTRA_TASK
                , trail
#endif
            );
            info_data_buf_size = strlen(info_data_buf);
            ret = write(info_data_fd, info_data_buf, info_data_buf_size);
            // ]
        }
        shape_free_record(rec);
    }
    shape_destroy(shape);
end:
    fsync(info_data_fd);
    close(info_data_fd);
    free(shp_buf); free(dbf_buf);
    if(info_log_fd != STDERR_FILENO)
        close(info_log_fd);
    return exit_status;
}