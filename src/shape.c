#include "shape.h"

#include <stdlib.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "strdupa.h"
#include <endian.h>
#include <stdio.h>
#include <sys/types.h>

// [ utils

static inline int shape_utils_is_leap_year(unsigned year) {
    return (((year % 4) == 0) && ((year % 100) != 0)) || ((year % 400) == 0);
}

char *shape_utils_strparts(char *dst, const void *parts, size_t nparts) {
    char *pdst = dst;
    const void *pparts = parts;
    int i;
    for(i = 0; i < nparts; i++) {
        char _tmp[11];
        sprintf(_tmp, "%d", *(int *) pparts);
        pparts += sizeof(int);
        strcpy(pdst, _tmp);
        pdst += strlen(pdst);
        if(i < (nparts - 1)) {
            *(pdst++) = ',';
            *(pdst++) = ' ';
        }
    }
    *pdst = '\0';
    return dst;
}

char *shape_utils_strcoordsxy(char *dst, const void *coords, size_t ncoords){
    char *pdst = dst;
    const void *pcoords = coords;
    int i;
    for(i = 0; i < ncoords; i++) {
        (*pdst++) = '(';
        char _tmp[11];
        sprintf(_tmp, "%.6f", *(double *) pcoords);
        strcpy(pdst, _tmp);
        pcoords += sizeof(double);
        pdst += strlen(_tmp);
        *(pdst++) = ';';
        *(pdst++) = ' ';
        sprintf(_tmp, "%.6f", *(double *) pcoords);
        strcpy(pdst, _tmp);
        pcoords += sizeof(double);
        pdst += strlen(_tmp);
        (*pdst++) = ')';
        if(i < (ncoords - 1)) {
            *(pdst++) = ',';
            *(pdst++) = ' ';
        }
    }
    *pdst = '\0';
    return dst;
}
// ]

#define _SHAPE_REAL_SIZE 65536
#define _SHAPE_RECORD_REAL_SIZE 65536

#define ledtoh(x) (x);

// [ Shape file related
#define SHP_INTEGER_LENGTH 4UL
#define SHP_DOUBLE_LENGTH 8UL
#define SHP_FILE_SIGNATURE 9994
#define SHP_FILE_SIGNATURE_IS_VALID(signature) ((signature) == SHP_FILE_SIGNATURE)
#define SHP_FILE_HEADER_SIZE 100UL
#define SHP_FILE_VERSION 1000
#define SHP_FILE_VERSION_IS_VALID(version) ((version) == SHP_FILE_VERSION)

ssize_t _shape_shp_decode_file_header(Shape *self) {
    ssize_t ret = 0;
    void *pbuf = self->shp_pbuffer;    
    int32_t raw_int32;
    double raw_double;
    // [
    // Magic, BE
    int got_signature;
    memcpy(&raw_int32, pbuf, SHP_INTEGER_LENGTH);
    got_signature = be32toh(raw_int32);
    if(!SHP_FILE_SIGNATURE_IS_VALID(got_signature))
        goto fail;
    // signature and skip next 5 integer-length unused fields
    pbuf += SHP_INTEGER_LENGTH * 6;
    ret += SHP_INTEGER_LENGTH * 6; // 24
    // File length in words, BE
    size_t got_length = 0;
    memcpy(&raw_int32, pbuf, SHP_INTEGER_LENGTH);
    got_length = (be32toh(raw_int32) << 1);
    pbuf += SHP_INTEGER_LENGTH;
    ret += SHP_INTEGER_LENGTH; // 28
    // Version, LE
    int got_version = 0;
    memcpy(&raw_int32, pbuf, SHP_INTEGER_LENGTH);
    got_version = le32toh(raw_int32);
    if(!SHP_FILE_VERSION_IS_VALID(got_version))
        goto fail;
    pbuf += SHP_INTEGER_LENGTH;
    ret += SHP_INTEGER_LENGTH;  // 32
    // Shape type, LE
    int got_shape_type;
    memcpy(&raw_int32, pbuf, SHP_INTEGER_LENGTH);
    got_shape_type = le32toh(raw_int32);
    if(!SHAPE_TYPE_IS_VALID(got_shape_type) || got_shape_type == SHAPE_TYPE_NULL)
        goto fail;
    pbuf += SHP_INTEGER_LENGTH;
    ret += SHP_INTEGER_LENGTH; // 36
    // [ Bounding Box, LE
    double got_box_x1 = 0.0, got_box_y1 = 0.0,
           got_box_x2 = 0.0, got_box_y2 = 0.0,
           got_box_z1 = 0.0, got_box_z2 = 0.0,
           got_box_m1 = 0.0, got_box_m2 = 0.0;
    memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
    got_box_x1 = ledtoh(raw_double);
    pbuf += SHP_DOUBLE_LENGTH;
    memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
    got_box_y1 = ledtoh(raw_double);
    pbuf += SHP_DOUBLE_LENGTH;
    memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
    got_box_x2 = ledtoh(raw_double);
    pbuf += SHP_DOUBLE_LENGTH;
    memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
    got_box_y2 = ledtoh(raw_double);
    pbuf += SHP_DOUBLE_LENGTH;
    ret += SHP_DOUBLE_LENGTH * 4; // 76
    if (SHAPE_TYPE_HAS_Z(got_shape_type)){
        memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
        got_box_z1 = ledtoh(raw_double);
        pbuf += SHP_DOUBLE_LENGTH;
        memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
        got_box_z2 = ledtoh(raw_double);
        pbuf += SHP_DOUBLE_LENGTH;
    } else {
        pbuf += SHP_DOUBLE_LENGTH * 2;
    }
    ret += SHP_DOUBLE_LENGTH * 2; // 92
    if (SHAPE_TYPE_HAS_M(got_shape_type)){
        memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
        got_box_m1 = ledtoh(raw_double);
        pbuf += SHP_DOUBLE_LENGTH;
        memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
        got_box_m2 = ledtoh(raw_double);
        pbuf += SHP_DOUBLE_LENGTH;
    } else {
        pbuf += SHP_DOUBLE_LENGTH * 2;
    }
    ret += SHP_DOUBLE_LENGTH * 2; // 100
    // ]
    // ]
    // [ Fill
    self->shp_type = got_shape_type;
    self->shp_box.xMin = got_box_x1;
    self->shp_box.xMax = got_box_x2;
    self->shp_box.yMin = got_box_y1;
    self->shp_box.yMax = got_box_y2;
    self->shp_box.zMin = got_box_z1;
    self->shp_box.zMax = got_box_z2;
    self->shp_box.mMin = got_box_m1;
    self->shp_box.mMax = got_box_m2;
    self->shp_full_size = got_length;
    self->shp_read_bytes += ret;
    self->shp_pbuffer = pbuf;
    // ]
#ifdef SHAPE_C_DEBUG
    printf("Signature: %d\n", got_signature);
    printf("Size: %lu\n", self->shp_full_size);
    printf("Version: %d\n", got_version);
    printf("Content Type: %d\n", self->shp_type);
    printf("Bounding Box: [(%.6f; %.6f; %.6f), (%.6f; %.6f; %.6f)]\n",
           self->shp_box.xMin, self->shp_box.yMin, self->shp_box.zMin,
           self->shp_box.xMax, self->shp_box.yMax, self->shp_box.zMax);
    printf("Bounding Box M: [%.6f, %.6f]\n",
           self->shp_box.mMin, self->shp_box.mMax);
#endif
    return ret;
fail:
    self->shp_type = SHAPE_TYPE_MALFORMED;
    return ret;
}

ssize_t _shape_shp_decode_record_header(Shape *self, ShapeRecord **out) {
    ssize_t ret = 0;
    void *pbuf = self->shp_pbuffer;
    ShapeRecord *pout = *out;
    int32_t raw_int32;
    int got_record_number;
    memcpy(&raw_int32, pbuf, SHP_INTEGER_LENGTH);
    got_record_number = be32toh(raw_int32);
    pbuf += SHP_INTEGER_LENGTH;
    ret += SHP_INTEGER_LENGTH;
    size_t got_content_length = 0;
    memcpy(&raw_int32, pbuf, SHP_INTEGER_LENGTH);
    got_content_length = (be32toh(raw_int32) << 1);
    if(got_content_length < 0)
        goto fail;
    pbuf += SHP_INTEGER_LENGTH;
    ret += SHP_INTEGER_LENGTH;
    // [ Fill the record
    pout->recordNumber = got_record_number;
    pout->contentLength = got_content_length;
    pout->contentType = 0; // reset content type
    self->shp_pbuffer = pbuf;
    self->shp_read_bytes += ret;
    // ]
#ifdef SHAPE_C_DEBUG
    printf("Record Number: %u\n", pout->recordNumber);
    printf("Content Length: %lu\n", pout->contentLength);
#endif
    return ret;
fail:
    self->shp_pbuffer += (2 * SHP_INTEGER_LENGTH);
    self->shp_read_bytes += (2 * SHP_INTEGER_LENGTH);
    pout->contentType = SHAPE_TYPE_MALFORMED;
    return ret;
}

ssize_t _shape_shp_decode_record_content_dummy(Shape *self, ShapeRecord **out) {
    ShapeRecord *pout = *out;
    ssize_t ret = pout->contentLength - SHP_INTEGER_LENGTH;
    self->shp_pbuffer += ret;
    self->shp_read_bytes += ret;
    return ret;
fail:
    self->shp_pbuffer += (pout->contentLength - SHP_INTEGER_LENGTH);
    self->shp_read_bytes += (pout->contentLength - SHP_INTEGER_LENGTH);
    pout->contentType = SHAPE_TYPE_MALFORMED;
    return ret;
}

ssize_t _shape_shp_decode_record_content_null(Shape *self, ShapeRecordNull **out) {
    ShapeRecordNull *pout = *out;
    pout->_unused = sizeof(struct _ShapeRecordNull);
    ssize_t ret = pout->contentLength - SHP_INTEGER_LENGTH;
    self->shp_pbuffer += ret;
    self->shp_read_bytes += ret;
    return ret;
fail:
    self->shp_pbuffer += (pout->contentLength - SHP_INTEGER_LENGTH);
    self->shp_read_bytes += (pout->contentLength - SHP_INTEGER_LENGTH);
    pout->contentType = SHAPE_TYPE_MALFORMED;
    return ret;
}

ssize_t _shape_shp_decode_record_content_point(Shape *self, ShapeRecordPoint **out) {
    ShapeRecordPoint *pout = *out;
    pout->_unused = sizeof(struct _ShapeRecordPoint);
    void *pbuf = self->shp_pbuffer;
    ssize_t ret = 0;
    double raw_double;
    double got_x = 0.0, got_y = 0.0;
    memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
    got_x = ledtoh(raw_double);
    pbuf += SHP_DOUBLE_LENGTH;
    memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
    got_y = ledtoh(raw_double);
    pbuf += SHP_DOUBLE_LENGTH;
    ret += SHP_DOUBLE_LENGTH * 2;
    pout->x = got_x;
    pout->y = got_y;
    self->shp_read_bytes += ret;
#ifdef SHAPE_C_DEBUG
    printf("Point: (%.6f; %.6f)\n", pout->x, pout->y);
#endif
    return ret;
fail:
    self->shp_pbuffer += (pout->contentLength - SHP_INTEGER_LENGTH);
    self->shp_read_bytes += (pout->contentLength - SHP_INTEGER_LENGTH);
    pout->contentType = SHAPE_TYPE_MALFORMED;
    return ret;
}

ssize_t _shape_shp_decode_record_content_multipoint(Shape *self, ShapeRecordMultipoint **out) {
    ssize_t ret = 0;
    ShapeRecordMultipoint *pout = *out;
    pout->_unused = sizeof(struct _ShapeRecordMultipoint);
    void *pbuf = self->shp_pbuffer;

    int32_t raw_int32;
    double raw_double;

    double got_box_x1 = 0.0, got_box_y1 = 0.0,
           got_box_x2 = 0.0, got_box_y2 = 0.0;
    memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
    got_box_x1 = ledtoh(raw_double);
    pbuf += SHP_DOUBLE_LENGTH;
    ret += SHP_DOUBLE_LENGTH;
    memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
    got_box_y1 = ledtoh(raw_double);
    pbuf += SHP_DOUBLE_LENGTH;
    ret += SHP_DOUBLE_LENGTH;
    memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
    got_box_x2 = ledtoh(raw_double);
    pbuf += SHP_DOUBLE_LENGTH;
    ret += SHP_DOUBLE_LENGTH;
    memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
    got_box_y2 = ledtoh(raw_double);
    pbuf += SHP_DOUBLE_LENGTH;
    ret += SHP_DOUBLE_LENGTH;

    int got_numpoints;
    size_t got_points_offset, got_unused;
    memcpy(&raw_int32, pbuf, SHP_INTEGER_LENGTH);
    got_numpoints = le32toh(raw_int32);
    if(got_numpoints < 1)
        goto fail;
    got_points_offset = sizeof(struct _ShapeRecordMultipoint);
    pbuf += SHP_INTEGER_LENGTH;
    ret += SHP_INTEGER_LENGTH;
    got_unused = got_points_offset + got_numpoints * sizeof(SHAPE_COORDS_XY);

    void *got_data, *pgot_data;
    size_t got_data_size;
    got_data_size = got_numpoints * sizeof(SHAPE_COORDS_XY);
    got_data = alloca(got_data_size);
    pgot_data = got_data;
    int i;
    for(i = 0; i < got_numpoints; i++) {
        double got_point_x, got_point_y;
        memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
        got_point_x = ledtoh(raw_double);
        memcpy(pgot_data, &got_point_x, sizeof(double));
        pgot_data += sizeof(double);
        pbuf += SHP_DOUBLE_LENGTH;
        ret += SHP_DOUBLE_LENGTH;
        memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
        got_point_y = ledtoh(raw_double);
        memcpy(pgot_data, &got_point_y, sizeof(double));
        pgot_data += sizeof(double);
        pbuf += SHP_DOUBLE_LENGTH;
        ret += SHP_DOUBLE_LENGTH;
    }

    pout->box.xMin = got_box_x1;
    pout->box.yMin = got_box_y1;
    pout->box.xMax = got_box_x2;
    pout->box.yMax = got_box_y2;
    pout->numPoints = got_numpoints;
    pout->_points = got_points_offset;
    pout->points = SHAPE_RECORD_POINTS_PTR(pout);
    memcpy(SHAPE_RECORD_UNUSED_PTR(pout), got_data, got_data_size);
    pout->_unused = got_unused;
    self->shp_read_bytes += (pout->contentLength - SHP_INTEGER_LENGTH);
    self->shp_pbuffer += (pout->contentLength - SHP_INTEGER_LENGTH);

#ifdef SHAPE_C_DEBUG
    printf("Bounding Box: [(%.6f; %.6f), (%.6f; %.6f)]\n", pout->box.xMin, pout->box.yMin,
                                                           pout->box.xMax, pout->box.yMax);
    printf("Number of Points: %d\n", pout->numPoints);
    // printf("Points Offset: %lu\n", pout->_points);
    char _tmp[4096];
    printf("Points: %s\n", shape_utils_strcoordsxy(_tmp, pout->points, pout->numPoints)); 
#endif
    return ret;
fail:
    self->shp_pbuffer += (pout->contentLength - SHP_INTEGER_LENGTH);
    self->shp_read_bytes += (pout->contentLength - SHP_INTEGER_LENGTH);
    pout->contentType = SHAPE_TYPE_MALFORMED;
    return ret;
}

ssize_t _shape_shp_decode_record_content_polyline(Shape *self, ShapeRecordPolyline **out) {
    ssize_t ret = 0;
    ShapeRecordPolyline *pout = *out;
    pout->_unused = sizeof(struct _ShapeRecordPolyline);
    void *pbuf = self->shp_pbuffer;

    int32_t raw_int32;
    double raw_double;

    double got_box_x1 = 0.0, got_box_y1 = 0.0,
           got_box_x2 = 0.0, got_box_y2 = 0.0;
    memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
    got_box_x1 = ledtoh(raw_double);
    pbuf += SHP_DOUBLE_LENGTH;
    ret += SHP_DOUBLE_LENGTH;
    memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
    got_box_y1 = ledtoh(raw_double);
    pbuf += SHP_DOUBLE_LENGTH;
    ret += SHP_DOUBLE_LENGTH;
    memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
    got_box_x2 = ledtoh(raw_double);
    pbuf += SHP_DOUBLE_LENGTH;
    ret += SHP_DOUBLE_LENGTH;
    memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
    got_box_y2 = ledtoh(raw_double);
    pbuf += SHP_DOUBLE_LENGTH;
    ret += SHP_DOUBLE_LENGTH;

    int got_numparts, got_numpoints;
    size_t got_parts_offset, got_points_offset, got_unused;
    memcpy(&raw_int32, pbuf, SHP_INTEGER_LENGTH);
    got_numparts = le32toh(raw_int32);
    if(got_numparts < 1)
        goto fail;
    got_parts_offset = sizeof(struct _ShapeRecordPolyline);
    pbuf += SHP_INTEGER_LENGTH;
    ret += SHP_INTEGER_LENGTH;
    memcpy(&raw_int32, pbuf, SHP_INTEGER_LENGTH);
    got_numpoints = le32toh(raw_int32);
    if(got_numpoints < 2)
        goto fail;
    got_points_offset = got_parts_offset + got_numparts * sizeof(int);
    pbuf += SHP_INTEGER_LENGTH;
    ret += SHP_INTEGER_LENGTH;
    got_unused = got_points_offset + got_numpoints * sizeof(SHAPE_COORDS_XY);

    void *got_data, *pgot_data;
    size_t got_data_size;

    got_data_size = got_numparts * sizeof(int) + got_numpoints * sizeof(SHAPE_COORDS_XY);
    got_data = alloca(got_data_size);
    pgot_data = got_data;
    int i;
    for(i = 0; i < got_numparts; i++) {
        int got_part;
        memcpy(&raw_int32, pbuf, SHP_INTEGER_LENGTH);
        got_part = le32toh(raw_int32);
        memcpy(pgot_data, &got_part, sizeof(int));
        pgot_data += sizeof(int);
        pbuf += SHP_INTEGER_LENGTH;
        ret += SHP_INTEGER_LENGTH;
    }
    for(i = 0; i < got_numpoints; i++) {
        double got_point_x, got_point_y;
        memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
        got_point_x = ledtoh(raw_double);
        memcpy(pgot_data, &got_point_x, sizeof(double));
        pgot_data += sizeof(double);
        pbuf += SHP_DOUBLE_LENGTH;
        ret += SHP_DOUBLE_LENGTH;
        memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
        got_point_y = ledtoh(raw_double);
        memcpy(pgot_data, &got_point_y, sizeof(double));
        pgot_data += sizeof(double);
        pbuf += SHP_DOUBLE_LENGTH;
        ret += SHP_DOUBLE_LENGTH;
    }

    pout->box.xMin = got_box_x1;
    pout->box.yMin = got_box_y1;
    pout->box.xMax = got_box_x2;
    pout->box.yMax = got_box_y2;
    pout->numParts = got_numparts;
    pout->numPoints = got_numpoints;
    pout->_parts = got_parts_offset;
    pout->_points = got_points_offset;
    pout->parts = SHAPE_RECORD_PARTS_PTR(pout);
    pout->points = SHAPE_RECORD_POINTS_PTR(pout);
    memcpy(SHAPE_RECORD_UNUSED_PTR(pout), got_data, got_data_size);
    pout->_unused = got_unused;
    self->shp_read_bytes += (pout->contentLength - SHP_INTEGER_LENGTH);
    self->shp_pbuffer += (pout->contentLength - SHP_INTEGER_LENGTH);

#ifdef SHAPE_C_DEBUG
    printf("Bounding Box: [(%.6f; %.6f), (%.6f; %.6f)]\n", pout->box.xMin, pout->box.yMin,
                                                           pout->box.xMax, pout->box.yMax);
    printf("Number of Parts: %d\n", pout->numParts);
    printf("Number of Points: %d\n", pout->numPoints);
    // printf("Parts Offset: %lu\n", pout->_parts);
    // printf("Points Offset: %lu\n", pout->_points);
    char _tmp[4096];
    printf("Parts: %s\n", shape_utils_strparts(_tmp, pout->parts, pout->numParts));
    printf("Points: %s\n", shape_utils_strcoordsxy(_tmp, pout->points, pout->numPoints)); 
#endif
    return ret;
fail:
    self->shp_pbuffer += (pout->contentLength - SHP_INTEGER_LENGTH);
    self->shp_read_bytes += (pout->contentLength - SHP_INTEGER_LENGTH);
    pout->contentType = SHAPE_TYPE_MALFORMED;
    return ret;
}

ssize_t _shape_shp_decode_record_content_polygon(Shape *self, ShapeRecordPolygon **out) {
    ssize_t ret = 0;
    ShapeRecordPolygon *pout = *out;
    pout->_unused = sizeof(struct _ShapeRecordPolygon);
    void *pbuf = self->shp_pbuffer;

    int32_t raw_int32;
    double raw_double;

    double got_box_x1 = 0.0, got_box_y1 = 0.0,
           got_box_x2 = 0.0, got_box_y2 = 0.0;
    memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
    got_box_x1 = ledtoh(raw_double);
    pbuf += SHP_DOUBLE_LENGTH;
    ret += SHP_DOUBLE_LENGTH;
    memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
    got_box_y1 = ledtoh(raw_double);
    pbuf += SHP_DOUBLE_LENGTH;
    ret += SHP_DOUBLE_LENGTH;
    memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
    got_box_x2 = ledtoh(raw_double);
    pbuf += SHP_DOUBLE_LENGTH;
    ret += SHP_DOUBLE_LENGTH;
    memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
    got_box_y2 = ledtoh(raw_double);
    pbuf += SHP_DOUBLE_LENGTH;
    ret += SHP_DOUBLE_LENGTH;

    int got_numparts, got_numpoints;
    size_t got_parts_offset, got_points_offset, got_unused;
    memcpy(&raw_int32, pbuf, SHP_INTEGER_LENGTH);
    got_numparts = le32toh(raw_int32);
    if(got_numparts < 1)
        goto fail;
    got_parts_offset = pout->_unused;
    pbuf += SHP_INTEGER_LENGTH;
    ret += SHP_INTEGER_LENGTH;
    memcpy(&raw_int32, pbuf, SHP_INTEGER_LENGTH);
    got_numpoints = le32toh(raw_int32);
    if(got_numpoints < 3)
        goto fail;
    got_points_offset = pout->_unused + got_numparts * sizeof(int);
    pbuf += SHP_INTEGER_LENGTH;
    ret += SHP_INTEGER_LENGTH;
    got_unused = got_points_offset + got_numpoints * sizeof(SHAPE_COORDS_XY);

    void *got_data, *pgot_data;
    size_t got_data_size;

    got_data_size = got_numparts * sizeof(int) + got_numpoints * sizeof(SHAPE_COORDS_XY);
    got_data = alloca(got_data_size);
    pgot_data = got_data;
    int i;
    for(i = 0; i < got_numparts; i++) {
        int got_part;
        memcpy(&raw_int32, pbuf, SHP_INTEGER_LENGTH);
        got_part = le32toh(raw_int32);
        memcpy(pgot_data, &got_part, sizeof(int));
        pgot_data += sizeof(int);
        pbuf += SHP_INTEGER_LENGTH;
        ret += SHP_INTEGER_LENGTH;
    }
    for(i = 0; i < got_numpoints; i++) {
        double got_point_x, got_point_y;
        memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
        got_point_x = ledtoh(raw_double);
        memcpy(pgot_data, &got_point_x, sizeof(double));
        pgot_data += sizeof(double);
        pbuf += SHP_DOUBLE_LENGTH;
        ret += SHP_DOUBLE_LENGTH;
        memcpy(&raw_double, pbuf, SHP_DOUBLE_LENGTH);
        got_point_y = ledtoh(raw_double);
        memcpy(pgot_data, &got_point_y, sizeof(double));
        pgot_data += sizeof(double);
        pbuf += SHP_DOUBLE_LENGTH;
        ret += SHP_DOUBLE_LENGTH;
    }

    pout->box.xMin = got_box_x1;
    pout->box.yMin = got_box_y1;
    pout->box.xMax = got_box_x2;
    pout->box.yMax = got_box_y2;
    pout->numParts = got_numparts;
    pout->numPoints = got_numpoints;
    pout->_parts = got_parts_offset;
    pout->_points = got_points_offset;
    pout->parts = SHAPE_RECORD_PARTS_PTR(pout);
    pout->points = SHAPE_RECORD_POINTS_PTR(pout);
    memcpy(SHAPE_RECORD_UNUSED_PTR(pout), got_data, got_data_size);
    pout->_unused = got_unused;
    self->shp_read_bytes += (pout->contentLength - SHP_INTEGER_LENGTH);
    self->shp_pbuffer += (pout->contentLength - SHP_INTEGER_LENGTH);

#ifdef SHAPE_C_DEBUG
    printf("Bounding Box: [(%.6f; %.6f), (%.6f; %.6f)]\n", pout->box.xMin, pout->box.yMin,
                                                           pout->box.xMax, pout->box.yMax);
    printf("Number of Parts: %d\n", pout->numParts);
    printf("Number of Points: %d\n", pout->numPoints);
    // printf("Parts Offset: %lu\n", pout->_parts);
    // printf("Points Offset: %lu\n", pout->_points);
    char _tmp[4096];
    printf("Parts: %s\n", shape_utils_strparts(_tmp, pout->parts, pout->numParts));
    printf("Points: %s\n", shape_utils_strcoordsxy(_tmp, pout->points, pout->numPoints));
#endif
    return ret;
fail:
    self->shp_pbuffer += (pout->contentLength - SHP_INTEGER_LENGTH);
    self->shp_read_bytes += (pout->contentLength - SHP_INTEGER_LENGTH);
    pout->contentType = SHAPE_TYPE_MALFORMED;
    return ret;
}

ssize_t _shape_shp_decode_record_content_pointz(Shape *self, ShapeRecord **out);
ssize_t _shape_shp_decode_record_content_multipointz(Shape *self, ShapeRecord **out);
ssize_t _shape_shp_decode_record_content_polylinez(Shape *self, ShapeRecord **out);
ssize_t _shape_shp_decode_record_content_polygonz(Shape *self, ShapeRecord **out);

ssize_t _shape_shp_decode_record_content_pointm(Shape *self, ShapeRecord **out);
ssize_t _shape_shp_decode_record_content_multipointm(Shape *self, ShapeRecord **out);
ssize_t _shape_shp_decode_record_content_polylinem(Shape *self, ShapeRecord **out);
ssize_t _shape_shp_decode_record_content_polygonm(Shape *self, ShapeRecord **out);

ssize_t _shape_shp_decode_record_content_multipatch(Shape *self, ShapeRecord **out);

ssize_t _shape_shp_decode_record_content(Shape *self, ShapeRecord **out) {
    ssize_t ret = 0;
    void *pbuf = self->shp_pbuffer;
    ShapeRecord *pout = *out;
    pout->_unused = sizeof(struct _ShapeRecord);
    // Stop processing if it has been setted before
    if(pout->contentType == SHAPE_TYPE_MALFORMED)
        return ret;
    int32_t raw_int32;
    // ContentType, LE
    int got_shape_type;
    memcpy(&raw_int32, pbuf, SHP_INTEGER_LENGTH);
    got_shape_type = le32toh(raw_int32);
    if(!SHAPE_TYPE_IS_VALID(got_shape_type)) {
        // It looks unhealthy. Has a doctor seen it?
        self->shp_pbuffer += pout->contentLength;
        pout->contentType = SHAPE_TYPE_MALFORMED;
        return ret;
    }
    ret += SHP_INTEGER_LENGTH;
    pbuf += SHP_INTEGER_LENGTH;
    self->shp_pbuffer = pbuf;
    pout->contentType = got_shape_type;
    self->shp_read_bytes += ret;
#ifdef SHAPE_C_DEBUG
    printf("Content Type: %u\n", pout->contentType);
#endif
    // Now, I know this kind of shit, lets do something with it :P
    ssize_t subret;
#if SHAPE_TYPE_IS_IMPLEMENTED(SHAPE_TYPE_NULL)
    if(pout->contentType == SHAPE_TYPE_NULL) {
        subret = _shape_shp_decode_record_content_null(self, (ShapeRecordNull **) &pout);
    } else
#endif
#if SHAPE_TYPE_IS_IMPLEMENTED(SHAPE_TYPE_POINT)
    if(pout->contentType == SHAPE_TYPE_POINT) {
        subret = _shape_shp_decode_record_content_point(self, (ShapeRecordPoint **) &pout);
    } else
#endif
#if SHAPE_TYPE_IS_IMPLEMENTED(SHAPE_TYPE_POLYLINE)
    if(pout->contentType == SHAPE_TYPE_POLYLINE) {
        subret = _shape_shp_decode_record_content_polyline(self, (ShapeRecordPolyline **) &pout);
    } else
#endif
#if SHAPE_TYPE_IS_IMPLEMENTED(SHAPE_TYPE_POLYGON)
    if(pout->contentType == SHAPE_TYPE_POLYGON) {
        subret = _shape_shp_decode_record_content_polygon(self, (ShapeRecordPolygon **) &pout);
    } else
#endif
#if SHAPE_TYPE_IS_IMPLEMENTED(SHAPE_TYPE_MULTIPOINT)
    if(pout->contentType == SHAPE_TYPE_MULTIPOINT) {
        subret = _shape_shp_decode_record_content_multipoint(self, (ShapeRecordMultipoint **) &pout);
    } else
#endif
#if SHAPE_TYPE_IS_IMPLEMENTED(SHAPE_TYPE_POINTZ)
    if(pout->contentType == SHAPE_TYPE_POINTZ) {
        subret = _shape_shp_decode_record_content_pointz(self, (ShapeRecordPointZ **) &pout);
    } else
#endif
#if SHAPE_TYPE_IS_IMPLEMENTED(SHAPE_TYPE_POLYLINEZ)
    if(pout->contentType == SHAPE_TYPE_POLYLINEZ) {
        subret = _shape_shp_decode_record_content_polylinez(self, (ShapeRecordPolylinez **) &pout);
    } else
#endif
#if SHAPE_TYPE_IS_IMPLEMENTED(SHAPE_TYPE_POLYGONZ)
    if(pout->contentType == SHAPE_TYPE_POLYGONZ) {
        subret = _shape_shp_decode_record_content_polygonz(self, (ShapeRecordPolygonz **) &pout);
    } else
#endif
#if SHAPE_TYPE_IS_IMPLEMENTED(SHAPE_TYPE_MULTIPOINTZ)
    if(pout->contentType == SHAPE_TYPE_MULTIPOINTZ) {
        subret = _shape_shp_decode_record_content_multipointz(self, (ShapeRecordMultipointZ **) &pout);
    } else
#endif
#if SHAPE_TYPE_IS_IMPLEMENTED(SHAPE_TYPE_POINTM)
    if(pout->contentType == SHAPE_TYPE_POINTM) {
        subret = _shape_shp_decode_record_content_pointm(self, (ShapeRecordPointM **) &pout);
    } else
#endif
#if SHAPE_TYPE_IS_IMPLEMENTED(SHAPE_TYPE_POLYLINEM)
    if(pout->contentType == SHAPE_TYPE_POLYLINEM) {
        subret = _shape_shp_decode_record_content_polylinem(self, (ShapeRecordPolylineM **) &pout);
    } else
#endif
#if SHAPE_TYPE_IS_IMPLEMENTED(SHAPE_TYPE_POLYGONM)
    if(pout->contentType == SHAPE_TYPE_POLYGONM) {
        subret = _shape_shp_decode_record_content_polygonm(self, (ShapeRecordPolygonM **) &pout);
    } else
#endif
#if SHAPE_TYPE_IS_IMPLEMENTED(SHAPE_TYPE_MULTIPOINTM)
    if(pout->contentType == SHAPE_TYPE_MULTIPOINTM) {
        subret = _shape_shp_decode_record_content_multipointm(self, (ShapeRecordMultipointM **) &pout);
    } else
#endif
#if SHAPE_TYPE_IS_IMPLEMENTED(SHAPE_TYPE_MULTIPATCH)
    if(pout->contentType == SHAPE_TYPE_MULTIPATCH) {
        subret = _shape_shp_decode_record_content_multipatch(self, (ShapeRecordMultipatch **) &pout);
    } else
#endif
    {
        // I haven't implemented it yet. I'll skip it, that's all I can do for you. Really sorry.
        subret = _shape_shp_decode_record_content_dummy(self, &pout);
    }
    ret += subret;
    return ret;
}
// ]

// [ dBase file related

// [ Visual FoxPro files have DBC description after header
// https://www.clicketyclick.dk/databases/xbase/format/dbf.html
#define DBF_SIGNATURE_VFOXPRO_1 0x30
#define DBF_SIGNATURE_VFOXPRO_2 0x31
#define DBF_VFOXPRO(signature) ( \
    (signature) == DBF_SIGNATURE_VFOXPRO_1 || \
    (signature) == DBF_SIGNATURE_VFOXPRO_2    \
)
// ]

// [ dBase7 files have different header
// http://www.dbase.com/KnowledgeBase/db7_file_fmt.htm
#define DBF_SIGNATURE_DB7VMSK 0x07
#define DBF_SIGNATURE_DB7VER 0x04
#define DBF_DBASE7(signature) ( ((signature) & DBF_SIGNATURE_DB7VMSK) == DBF_SIGNATURE_DB7VER )
// ]

#define DBF_SIGNATURE_DBMS_MASK 0x07
#define DBF_SIGNATURE_MEMO_MASK 0x08
#define DBF_SIGNATURE_SQL_MASK  0x60
#define DBF_SIGNATURE_DBT_MASK  0x70
#define DBF_HAS_MEMO(signature) ( (signature) & (DBF_SIGNATURE_MEMO_MASK | DBF_SIGNATURE_DBT_MASK) != 0 )

static inline int DBF_SIGNATURE_IS_VALID(uint8_t data[4]) {
    if(data[0] < 2)
        goto fail;
    if(data[2] < 1 || data[2] > 12)
        goto fail;
    switch(data[2]) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            if(data[2] < 1 || data[2] > 31)
                goto fail;
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            if(data[2] < 1 || data[2] > 30)
                goto fail;
            break;
        case 2:
            if(data[2] < 1 || data[2] > (shape_utils_is_leap_year(data[1] + 1900) ? 29 : 28))
                goto fail;
            break;
    }
    return 1;
fail:
    return 0;
}

ssize_t _shape_dbf_decode_field_descriptors(Shape *self) {
    ssize_t ret = 0;
    void *pbuf = self->dbf_pbuffer;
    uint8_t raw_uint8, *raw_data, *praw_data;
    uint16_t raw_uint16;
    uint32_t raw_uint32;
    size_t got_field_count = 0;
    void *pattrs = self->dbf_attrs;
    for(;;) {
        // 0
        raw_uint8 = *(uint8_t *) pbuf;
        if(raw_uint8 == 0x0d)
            break;
        char *got_field_name;
        raw_data = alloca(11);
        memcpy(raw_data, pbuf, 11);
        got_field_name = strdupa(raw_data);
        pbuf += 11;
        ret += 11;
        // 11
        int got_field_type;
        raw_uint8 = *(uint8_t *) pbuf;
        got_field_type = raw_uint8;
        pbuf += 1;
        ret += 1;
        // 12
        pbuf += 4;
        ret += 4;
        // 16
        // Field length
        size_t got_field_length = 0, got_field_decimal_count = 0;
        if(got_field_type == 'C') {
            memcpy(&raw_uint16, pbuf, sizeof(uint16_t));
            got_field_length = le16toh(raw_uint16);
            pbuf += sizeof(uint16_t);
            ret += sizeof(uint16_t);
        } else {
            raw_uint8 = *(uint8_t *) pbuf;
            got_field_length = raw_uint8;
            pbuf += 1;
            ret += 1;
            raw_uint8 = *(uint8_t *) pbuf;
            got_field_decimal_count = raw_uint8;
            pbuf += 1;
            ret += 1;
        }
        // 18
        pbuf += 14;
        ret += 14;
        got_field_count++;
        ShapeAttribute *got_attr = alloca(sizeof(struct _ShapeAttribute));
        strcpy(got_attr->attrName, got_field_name);
        got_attr->attrType = got_field_type;
        got_attr->attrSize = got_field_length;
        got_attr->attrDecimalCount = got_field_decimal_count;
        got_attr->value = NULL;
        memcpy(pattrs, got_attr, sizeof(struct _ShapeAttribute));
        pattrs += sizeof(struct _ShapeAttribute);
#ifdef SHAPE_C_DEBUG
        printf("Field %lu Name: %s\n", got_field_count, got_field_name);
        printf("Field %lu Type: %c\n", got_field_count, got_field_type);
        printf("Field %lu Length: %lu\n", got_field_count, got_field_length);
        printf("Field %lu Decimal Count: %lu\n", got_field_count, got_field_decimal_count);
#endif
    }
    self->dbf_field_count = got_field_count;
    self->dbf_pbuffer = pbuf;
    self->dbf_read_bytes += ret;
#ifdef SHAPE_C_DEBUG
    printf("Fields Count: %lu\n", got_field_count);
#endif
    return ret;
}

ssize_t _shape_dbf_decode_field_descriptors_db7(Shape *self) {
    ssize_t ret = 0;
    void *pbuf = self->dbf_pbuffer;
    uint8_t raw_uint8, *raw_data, *praw_data;
    uint16_t raw_uint16;
    uint32_t raw_uint32;
    size_t got_field_count = 0;
    void *pattrs = self->dbf_attrs;
    for(;;) {
        uint8_t raw_uint8 = *(uint8_t *) pbuf;
        if(raw_uint8 == 0x0d)
            break;
        // 0
        char *got_field_name;
        raw_data = alloca(32);
        memcpy(raw_data, pbuf, 32);
        got_field_name = strdupa(raw_data);
        pbuf += 32;
        ret += 32;
        // 32
        int got_field_type;
        raw_uint8 = *(uint8_t *) pbuf;
        got_field_type = raw_uint8;
        pbuf += 1;
        ret += 1;
        // 33
        size_t got_field_length = 0, got_field_decimal_count = 0;
        if(got_field_type == 'C') {
            memcpy(&raw_uint16, pbuf, sizeof(uint16_t));
            got_field_length = le16toh(raw_uint16);
            pbuf += sizeof(uint16_t);
            ret += sizeof(uint16_t);
        } else {
            raw_uint8 = *(uint8_t *) pbuf;
            got_field_length = raw_uint8;
            pbuf += 1;
            ret += 1;
            raw_uint8 = *(uint8_t *) pbuf;
            got_field_decimal_count = raw_uint8;
            pbuf += 1;
            ret += 1;
        }
        // 35
        pbuf += 13;
        ret += 13;
        got_field_count++;
        ShapeAttribute *got_attr = alloca(sizeof(struct _ShapeAttribute));
        strcpy(got_attr->attrName, got_field_name);
        got_attr->attrType = got_field_type;
        got_attr->attrSize = got_field_length;
        got_attr->attrDecimalCount = got_field_decimal_count;
        got_attr->value = NULL;
        memcpy(pattrs, got_attr, sizeof(struct _ShapeAttribute));
        pattrs += sizeof(struct _ShapeAttribute);
#ifdef SHAPE_C_DEBUG
        printf("Field %lu Name: %s\n", got_field_count, got_field_name);
        printf("Field %lu Type: %c\n", got_field_count, got_field_type);
        printf("Field %lu Length: %lu\n", got_field_count, got_field_length);
        printf("Field %lu Decimal Count: %lu\n", got_field_count, got_field_decimal_count);
#endif
    }
    self->dbf_field_count = got_field_count;
    self->dbf_pbuffer = pbuf;
    self->dbf_read_bytes += ret;
#ifdef SHAPE_C_DEBUG
    printf("Fields Count: %lu\n", got_field_count);
#endif
    return ret;
}

ssize_t _shape_dbf_decode_file_header(Shape *self) {
    ssize_t ret = 0;
    void *pbuf = self->dbf_pbuffer;
    uint8_t raw_uint8, *raw_data, *praw_data;
    uint16_t raw_uint16;
    uint32_t raw_uint32;
    // 0
    // Signature including date, uint8_t[4]
    raw_data = alloca(4);
    int got_signature;
    unsigned got_mod_year, got_mod_month, got_mod_day;
    memcpy(raw_data, pbuf, 4);
    if(!DBF_SIGNATURE_IS_VALID(raw_data))
        goto fail;
    got_signature = raw_data[0];
    got_mod_year = raw_data[1] + 1900;
    got_mod_month = raw_data[2];
    got_mod_day = raw_data[3];
    pbuf += 4;
    ret += 4;
    // 4
    // Number of records, uint32_t, LE
    size_t got_record_count;
    memcpy(&raw_uint32, pbuf, sizeof(uint32_t));
    got_record_count = le32toh(raw_uint32);
    pbuf += sizeof(uint32_t);
    ret += sizeof(uint32_t);
    // 8
    // Header size, uint16_t, LE
    size_t got_header_size;
    memcpy(&raw_uint16, pbuf, sizeof(uint16_t));
    got_header_size = le16toh(raw_uint16);
    pbuf += sizeof(uint16_t);
    ret += sizeof(uint16_t);
    // 10
    // Record size including deletion flag, uint16_t, LE
    size_t got_record_size;
    memcpy(&raw_uint16, pbuf, sizeof(uint16_t));
    got_record_size = le16toh(raw_uint16);
    pbuf += sizeof(uint16_t);
    ret += sizeof(uint16_t);
    // 12
    pbuf += 17;
    ret += 17;
    // 29
    // Codepage, uint8_t
    int got_codepage;
    raw_uint8 = *(uint8_t *) pbuf;
    got_codepage = raw_uint8;
    // [ codepage related code
    // ]
    pbuf += 1;
    ret += 1;
    // 30
    // Reserved, uint8_t[2]
    pbuf += 2;
    ret += 2;
    // 32
    // dBase7 DBF file only (http://www.dbase.com/KnowledgeBase/db7_file_fmt.htm)
    if(DBF_DBASE7(got_signature)) {
        pbuf += 36;
        ret + 36;
        // 68
    }
    // Finish common header here
    self->dbf_malformed = 0;
    self->dbf_signature = got_signature;
    self->dbf_record_count = got_record_count;
    self->dbf_record_size = got_record_size;
    self->_dbf_attrs = sizeof(struct _Shape);
    self->dbf_attrs = ((void *) self + self->_dbf_attrs);

    self->dbf_pbuffer = pbuf;
    self->dbf_full_size = got_header_size + got_record_count * got_record_size + 1;
    self->dbf_read_bytes = ret;
#ifdef SHAPE_C_DEBUG
    printf("Signature: %d\n", self->dbf_signature);
    printf("Size: %lu\n", self->dbf_full_size);
    printf("Modified: %04u-%02u-%02u\n", got_mod_year, got_mod_month, got_mod_day);
    printf("Number of Records: %lu\n", self->dbf_record_count);
    printf("Record Size: %lu\n", self->dbf_record_size);
#endif
    // Field descriptors
    ssize_t subret;
    if(DBF_DBASE7(got_signature)) {
        subret = _shape_dbf_decode_field_descriptors_db7(self);
    } else {
        subret = _shape_dbf_decode_field_descriptors(self);
    }
    ret += subret;
    // Terminator, uint8_t, (0x0d)
    self->dbf_pbuffer += 1;
    self->dbf_read_bytes += 1;
    ret += 1;
    // Visual FoxPro DBF file has extra 264 bytes after header (https://www.clicketyclick.dk/databases/xbase/format/dbf.html)
    if(DBF_VFOXPRO(got_signature)) {
        ret += 264;
        self->dbf_pbuffer += 264;
        self->dbf_read_bytes += 264;
    }
    return ret;
fail:
    self->dbf_malformed = 1;
    return ret;
}

ssize_t _shape_dbf_decode_record_attrs_values(Shape *self, ShapeRecord **out) {
    ssize_t ret = 0;
    ShapeRecord *pout = *out;
    void *pbuf = self->dbf_pbuffer;
    uint8_t *raw_data, *praw_data;
    size_t raw_data_size = self->dbf_record_size;
    raw_data = alloca(raw_data_size);
    praw_data = raw_data;
    memcpy(raw_data, pbuf, raw_data_size);
    if(*praw_data != ' ')
        goto fail;
    praw_data += 1;
    void *got_data;
    got_data = alloca(raw_data_size + self->dbf_record_count);
    ShapeAttribute *attr = pout->attrs;
    size_t got_unused;
    int i;
    for(i = 0; i < pout->numAttrs; i++) {
        got_data = SHAPE_RECORD_UNUSED_PTR(pout);
        memset(got_data, 0, attr[i].attrSize + 1);
        memcpy(got_data, praw_data, attr[i].attrSize);
        praw_data += attr[i].attrSize;
        ret += attr[i].attrSize;
        attr[i].value = got_data;
        pout->_unused += (attr[i].attrSize + 1);
    }
    pbuf += raw_data_size;
    self->dbf_pbuffer = pbuf;
    self->dbf_read_bytes += raw_data_size;
    return ret;
fail:
    self->dbf_pbuffer += self->dbf_record_size;
    self->dbf_read_bytes += self->dbf_record_size;
    pout->attrsMalformed = 1;
    return ret;
}

ssize_t _shape_dbf_decode_record_attrs(Shape *self, ShapeRecord **out) {
    ssize_t ret = 0;
    ShapeRecord *pout = *out;
    pout->attrsMalformed = 0;
    size_t got_attrs_offset, got_unused;
    got_attrs_offset = pout->_unused;
    pout->_attrs = got_attrs_offset;
    pout->numAttrs = self->dbf_field_count;
    got_unused = got_attrs_offset + pout->numAttrs * sizeof(struct _ShapeAttribute);
    pout->attrs = SHAPE_RECORD_ATTRS_PTR(pout);
    pout->_unused = got_unused;
    memcpy(pout->attrs, self->dbf_attrs, pout->numAttrs * sizeof(struct _ShapeAttribute));
    ret = _shape_dbf_decode_record_attrs_values(self, &pout);
    return ret;
}
// ]

// [ API
Shape *shape_create() {
    Shape *self;
    if(self = (Shape *) calloc(1, _SHAPE_REAL_SIZE))
        return self;
    return NULL;
}

void shape_destroy(Shape *self) {
    if(!self)
        return;
    free(self);
}

int shape_load_data(Shape *self, void *shp_buffer, size_t shp_buffer_size, void *dbf_buffer, size_t dbf_buffer_size) {
    if(!self)
        return SHAPE_STATUS_ERR;
    if(!shp_buffer)
        return SHAPE_STATUS_ERR;
    ssize_t ret;
    if(shp_buffer) {
        self->shp_buffer = shp_buffer;
        self->shp_buffer_size = self->shp_buffer_size;
        self->shp_pbuffer = self->shp_buffer;
        self->shp_read_bytes = 0;
        self->_dbf_attrs = 0;
        self->dbf_attrs = NULL;
        // parse here shape header
        ret = _shape_shp_decode_file_header(self);
        if(self->shp_type == SHAPE_TYPE_MALFORMED)
            return SHAPE_STATUS_INVALID_SHP;
    }
    if(dbf_buffer) {
        self->dbf_buffer = dbf_buffer;
        self->dbf_buffer_size = dbf_buffer_size;
        self->dbf_pbuffer = self->dbf_buffer;
        self->dbf_read_bytes = 0;
        // parse here dbase header
        ret = _shape_dbf_decode_file_header(self);
        if(self->dbf_malformed)
            return SHAPE_STATUS_INVALID_DBF;
    }
    return SHAPE_STATUS_OK;
}

int shape_read_record(Shape *self, ShapeRecord **out) {
    if(!self || !out || !*out)
        return SHAPE_STATUS_ERR;
    if(self->shp_type == SHAPE_TYPE_MALFORMED)
        return SHAPE_STATUS_INVALID_SHP;
    if(self->shp_read_bytes >= self->shp_full_size)
        return SHAPE_STATUS_EOF;
    ssize_t ret;
    ret = _shape_shp_decode_record_header(self, out);
    if((*out)->contentType == SHAPE_TYPE_MALFORMED)
        return SHAPE_STATUS_INVALID_SHP;
    ret = _shape_shp_decode_record_content(self, out);
    if((*out)->contentType == SHAPE_TYPE_MALFORMED)
        return SHAPE_STATUS_INVALID_SHP;
    if(self->dbf_attrs) {
        if(self->dbf_read_bytes >= self->dbf_full_size)
            return SHAPE_STATUS_INVALID_DBF;
        ret = _shape_dbf_decode_record_attrs(self, out);
        if((*out)->attrsMalformed == 1)
            return SHAPE_STATUS_INVALID_DBF;
    }
    return SHAPE_STATUS_OK;
}

ShapeRecord *shape_alloc_record() {
    return (ShapeRecord *) calloc(1, _SHAPE_RECORD_REAL_SIZE);
}

const char *shape_get_record_attr(ShapeRecord *record, const char *attr, char *dst, size_t *attrsize, int *attrtype) {
    if(!record)
        return NULL;
    if(!record->attrs || record->attrsMalformed == 1)
        return NULL;
    const char *ret = NULL;
    int i;
    for(i = 0; i < record->numAttrs; i++) {
        if(strcmp(record->attrs[i].attrName, attr) == 0) {
            if(dst)
                ret = (const char *) memcpy(dst, record->attrs[i].value, record->attrs[i].attrSize + 1); // I store raw data plus zero byte in row
            else
                ret = (const char *) record->attrs[i].value;
            if(attrsize)
                *attrsize = record->attrs[i].attrSize;
            if(attrtype)
                *attrtype = record->attrs[i].attrType;
            break;
        }
    }
    return ret;
}

void shape_free_record(ShapeRecord *record) {
    if(!record)
        return;
    free(record);
}
// ]